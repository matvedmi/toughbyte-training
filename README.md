# Training repository for Git
This is a training repository

Clone it with SSH (you should add SSH public key in your account settings):

```
git clone git@bitbucket.org:matvedmi/toughbyte-training.git
```

or with HTTPS:
```
git clone https://matvedmi@bitbucket.org/matvedmi/toughbyte-training.git
```
